package com.test.consumer.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @Author: laz
 * @CreateTime: 2022-12-09  11:48
 * @Version: 1.0
 */
@FeignClient(value = "consumer-management")
public interface IConsumerClient {


    @GetMapping("/client/consumer/getByConsumerId/{id}")
    Object getByConsumerId(@PathVariable("id")Integer id);
}
