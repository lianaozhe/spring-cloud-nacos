package com.test.consumer.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @Author: laz
 * @CreateTime: 2022-12-09  10:10
 * @Version: 1.0
 */
@Data
@Accessors(chain = true)
@TableName("consumer")
public class Consumer {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("customer_name")
    private String customerName;

    private Integer age;

    private String sex;

}
