//package com.test.consumer.fallback;
//
//import com.test.consumer.feign.IConsumerClient;
//import org.springframework.stereotype.Component;
//
///**
// * @Author: laz
// * @CreateTime: 2022-12-09  17:10
// * @Version: 1.0
// */
//@Component
//public class ConsumerFallBack implements IConsumerClient {
//    @Override
//    public Object getByConsumerId(Integer id) {
//        return "服务异常";
//    }
//}
