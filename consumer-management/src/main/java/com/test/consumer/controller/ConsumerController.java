package com.test.consumer.controller;

import com.test.consumer.entity.Consumer;
import com.test.consumer.service.IConsumerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: laz
 * @CreateTime: 2022-12-09  11:09
 * @Version: 1.0
 */
@Api(tags = "客户模块")
@RestController
@RequestMapping("/consumer")
public class ConsumerController {


    @Autowired
    private IConsumerService consumerService;

    @ApiOperation(value = "通过客户id查询用户信息")
    @GetMapping("/getByConsumerId/{id}")
    public Object getByConsumerId(@PathVariable("id")Integer id){
        Object byConsumerId = consumerService.getByConsumerId(id);
        return byConsumerId;
    }

}
