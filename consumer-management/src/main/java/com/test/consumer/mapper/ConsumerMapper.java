package com.test.consumer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.test.consumer.entity.Consumer;

/**
 * @Author: laz
 * @CreateTime: 2022-12-09  10:51
 * @Version: 1.0
 */
public interface ConsumerMapper extends BaseMapper<Consumer> {

}
