package com.test.consumer.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.test.consumer.entity.Consumer;
import com.test.consumer.mapper.ConsumerMapper;
import com.test.consumer.service.IConsumerService;
import org.springframework.stereotype.Service;

/**
 * @Author: laz
 * @CreateTime: 2022-12-09  10:53
 * @Version: 1.0
 */
@Service
public class ConsumerServiceImpl extends ServiceImpl<ConsumerMapper, Consumer> implements IConsumerService {

    @Override
    public Object getByConsumerId(int id) {
        return this.baseMapper.selectById(id);
    }
}
