package com.test.consumer.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.test.consumer.entity.Consumer;

/**
 * @Author: laz
 * @CreateTime: 2022-12-09  10:51
 * @Version: 1.0
 */
public interface IConsumerService extends IService<Consumer> {

    Object getByConsumerId(int id);

}
