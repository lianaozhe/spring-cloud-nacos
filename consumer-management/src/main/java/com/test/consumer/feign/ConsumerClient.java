package com.test.consumer.feign;

import com.test.consumer.mapper.ConsumerMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: laz
 * @CreateTime: 2022-12-09  11:51
 * @Version: 1.0
 */
@RestController
@AllArgsConstructor
@Service
public class ConsumerClient implements IConsumerClient {

    private final ConsumerMapper consumerMapper;

    @Override
    public Object getByConsumerId(Integer id) {
        return consumerMapper.selectById(id);
    }
}
