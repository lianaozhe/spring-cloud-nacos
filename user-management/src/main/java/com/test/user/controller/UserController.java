package com.test.user.controller;

import com.test.user.service.IUserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: laz
 * @CreateTime: 2022-12-09  11:56
 * @Version: 1.0
 */
@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {


    private final IUserService userService;

    @RequestMapping("/getByConsumerId/{id}")
    public Object getByConsumerId(@PathVariable("id")Integer id){
        return userService.getByConsumerId(id);
    }

}
