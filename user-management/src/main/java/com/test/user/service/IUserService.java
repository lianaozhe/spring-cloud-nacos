package com.test.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.test.user.entity.User;

/**
 * @Author: laz
 * @CreateTime: 2022-12-09  10:49
 * @Version: 1.0
 */
public interface IUserService extends IService<User> {


    Object getByConsumerId(int id);

}
