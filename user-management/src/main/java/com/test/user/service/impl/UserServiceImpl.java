package com.test.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.test.consumer.feign.IConsumerClient;
import com.test.user.entity.User;
import com.test.user.mapper.UserMapper;
import com.test.user.service.IUserService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * @Author: laz
 * @CreateTime: 2022-12-09  10:50
 * @Version: 1.0
 */
@Service
@AllArgsConstructor
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    private final IConsumerClient consumerClient;

    @Override
    public Object getByConsumerId(int id) {
        return consumerClient.getByConsumerId(id);
    }
}
