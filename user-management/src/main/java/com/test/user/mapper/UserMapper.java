package com.test.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.test.user.entity.User;

/**
 * @Author: laz
 * @CreateTime: 2022-12-09  10:47
 * @Version: 1.0
 */
public interface UserMapper extends BaseMapper<User> {

}
