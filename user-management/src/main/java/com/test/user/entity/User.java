package com.test.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @Author: laz
 * @CreateTime: 2022-12-09  10:24
 * @Version: 1.0
 */
@Data
@Accessors(chain = true)
@TableName("consumer")
public class User {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("user_name")
    private Integer userName;

    private String mobile;
}
